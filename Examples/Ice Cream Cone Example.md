# Ice Cream Cones
Suppose you are buying an ice cream cone. You can choose whether to have a cake cone or a waffle cone, and whether to have chocolate, vanilla, or strawberry as your flavor. 
___

How many possible options do you have for your ice cream cone?
- $2 \cdot 3 = 6$

How about for 2 ice cream cones at different times?
- If you care about which is eaten first [[Sampling with replacement]]
	- $(2 \cdot 3)^2 = 36$
- If not [[Sampling without replacement]]
	- (x, y) = (y, x)
	- $6 \cdot 5 = 30$ ordered probabilities $(x,y)$ with $x \not= y$
	- Since $(x, y) = (y, x)$, then we have $\frac{30}{2} = 15$
	- Then add back the duplicate possibilities of $(x, x)$,  $6 \cdot 5 - 6 \cdot 6 = 6$
	- The total comes out to 21 possibilities that are not all equally likely.
		- This is because all the $(x, y) = (y, x)$ were collapsed