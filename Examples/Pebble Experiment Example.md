# Pebble Experiment
Randomly selecting one pebble from a bowl with 3 red and 3 blue pebbles: $\{Bl,Bl,Bl,Re,Re,Re\}$ with the same mass (have equal chance to be chosen)
___
Sample space is
$$S = \{Bl,Bl,Bl,Re,Re,Re\}$$
Then the events are $A, B \subseteq S$ where $A = \{Bl, Bl, Bl\}$ is the [[Event]] of selecting a blue pebble and $B = \{Re, Re, Re\}$ is selecting a red pebble.

### [[Event]]s
- The union $A \cup B$ is the event that either $A$ or $B$ occurs. In this case it is equal to $S$.
- The intersection $A \cap B$ is the event that occurs if and only if both $A$ and $B$ occur. In this case $A \cap B = \emptyset$ (also the case for all mutally exclusive events).
- The [[Complement]] $A^c$ is the event that occurs if and only if $A$ does not occur.

### [[Probability]]
Since $|A| = |B|$ and each pebble has equal weight then
$$P_{naive}(A) = P_{naive}(B) = \frac{3}{6}$$

To calculate joint probabilities
- $P_{naive}(A\cup B) = 1$ (since $A \cup B = S$)
- $P_{naive}(A \cap B) = 0$ (since $A \cap B = \emptyset$)

To calculate complement probability
$$P_{naive}(A^c) = \frac{|A^c|}{|S|} = \frac{|S| - |A|}{|S|} = 1 - \frac{|A|}{|S|} = 1 - P_{naive}(A)$$
