# Grouping Outcomes
___
First each result needs to be encoded into a [[Sample Space]], see [[Encoding Results]].

Now create a sequence of the encoded results which now has a defined numerical range.

In the example that we are looking at a sequence of coin flips
- Let $A_1$ be the [[Event]] that the first flip is heads, we can write it as a set:
$$A_1 = \{(1, s_2, ..., s_10): s_j \in \{0, 1\}\text{ for }2 \le j \le 10\}$$
- Let $A_j$ be the [[Event]] that the jth flip is heads for j = 2, 3,..., 10
- Let **B** be the [[Event]] that at least one flip was heads
$$B = \bigcap\limits_{j=1}^{10} A_j$$
- Let **C** be the [[Event]] that all flips were heads, we can write it as a set
$$C = \bigcup\limits_{j=1}^{10} A_j$$
- Let **D** be the [[Event]] that there were at least two consecutive heads, we can write it as a set
$$ D = \bigcup\limits_{j=1}^{9} (A_j \cap A_{j + 1})$$