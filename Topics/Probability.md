# Probability - the logic of uncertainty

Naive defintion
- Let **A** be an event for an experiment with a finite sample space **S**. The *naive probability* of **A** is
$$P_{naive} = \frac{|A|}{|S|} = \frac{\text{outcomes favorable to A}}{\text{total outcomes in S}}$$

$|A|$ is [[Cardinality]]
___
### When can the naive definition be used?

- When there is symmetry in the problem that makes the outcomes equally likely
- When the outcomes are equally likely by design
	- By design usually means in a simple random sample.
- When the definition serves as a null model

**Need to be able to count**
 - See [[Multiplication Rule]]
 - See [[Pebble Experiment Example]]
