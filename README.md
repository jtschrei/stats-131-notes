# Stats 131 Notes
Notes for Cordoba's Fall 2021 CSE 120 class at UCSC. Should be viewed and edited using [Obsidian MD](https://obsidian.md/).

All notes may be expanded on at any time as new information is taught from the course.
___

## File Layout
Files in these notes are organized into two main categories.

Topics
- The main ideas of the course that are essential to know to have a good grasp of the content.
- May be more of these than are stated in the lecture slides but all of it is required information.

Concepts
- Various concepts taught throughout the course with varying degrees of importance.
- Everything that should be known from the course but you could get away with not knowing some.

## Table of Contents
1. [[Grouping Outcomes]]
2. [[Probability]]
3. [[Sampling]]