# Cardinality
The size of a set or sequence.

Notation is $|A|$ to find the cardinality of set A.

Cardinality also exists for infinite sets.