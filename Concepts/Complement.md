# Complement
Notation is $A^c$

When $A \subseteq S$ then $A^c$ is equal to the set of all values in $S$ that are not in $A$.
