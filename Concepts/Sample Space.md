# Sample Space
The sample space $S$ of an experiment is the set of all possible outcomes of the experiment.
___
Sample spaces can be
- Finite
	- Integer [[Cardinality]]
- Countable
	- One to one correspondence to the integers (wtf?)
- Uncountable
	- Neither finite nor countable

