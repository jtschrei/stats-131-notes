# Multiplication Rule
*Theorem*
> Condiser a compound experiment consisting of two sub-experiments A and B. A has a possible outcomes and B has b possible outcomes. The compound experiment has ab possible outcomes.
___

- No chronological requirement
- Can be shown using a tree diagram to help understand