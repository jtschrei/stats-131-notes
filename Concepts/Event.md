# Event
An *event* $A$ is a subset of the [[Sample Space]] $S$, and we say that $A$ occurred if the actual outcome is in $A$.
___
### Notation
All events must satisfy
- $A \subseteq S$

And if A occurred then $s_{actual} \in A$ must be true

### Transformation of events
To transform two events $A, B \subseteq S$ (See also: [[Pebble Experiment Example]])

A or B
 - $A \cup B$

A and B
- $A \cap B$

not A ([[Complement]])
- $A^c$

A or B, but not both
- $(A \cap B^c) \cup (A^c \cap B)$

At least one of $A_1, \cdots, A_n$
- $A_1\cup\cdots\cup A_n$
- $\bigcup\limits_{i = 1}^{n}A_i$

All of $A_1, \cdots, A_n$
- $A_1\cap\cdots\cap A_n$
- $\bigcap\limits_{i = 1}^{n}A_i$

### Relationships between events
A implies B
- $A \subseteq B$

A and B are mutually exclusive
- $A \cap B = \emptyset$

$A_1, \cdots, A_n$ are a partition of S
- $A_1\cup\cdots\cup A_n = S, A_i\cap A_j = \emptyset\text{ for }i \not= j$

*Morgan's laws also apply*
