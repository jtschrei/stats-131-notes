# Sampling without replacement
*Theorem*
Consider *n* objects and making *k* choices from them, one at a time without replacement. Then there are
$n(n - 1)(n-2)...(n - k + 1)$ possible outcomes for $1 \le k \le n$ and 0 possibilities for $k > n$. By convention, $n(n - 1)(n-2)...(n - k + 1) = n$ for $k = 1$
___