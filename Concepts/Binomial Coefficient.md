# Binomial Coefficient
*Definition*
For any nonnegative integers $k$ and $n$, the *binomial coefficient* $\pmatrix{1\\2}$, read as "$n$ choose $k$"

#TODO on Wednesday